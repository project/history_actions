<?php

namespace Drupal\history_actions\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\node\NodeInterface;

/**
 * Action description.
 *
 * @Action(
 *   id = "history_actions_read",
 *   label = @Translation("Mark as read."),
 *   type = "node"
 * )
 */
class MarkNodeAsRead extends ActionBase {

  use MarkNodeTrait;

  /**
   * {@inheritDoc}
   */
  public function execute($node = NULL) {
    if ($node instanceof NodeInterface) {
      $this->historyWriteTimestamp($node->id(), \Drupal::time()->getRequestTime());
      $this->invalidateListCacheTag($node);
    }
  }

}
