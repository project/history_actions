<?php

namespace Drupal\history_actions\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;

trait MarkNodeTrait {

  public function invalidateListCacheTag(NodeInterface $node) {
    Cache::invalidateTags($node->getEntityType()->getListCacheTags());
  }

  /**
   * Updates 'last viewed' timestamp of the specified entity for the current user.
   *
   * Copy of history_write.
   *
   * @param $nid
   *   The node ID that has been read.
   * @param $timestamp
   *   The timestamp to write.
   * @param $account
   *   (optional) The user account to update the history for. Defaults to the
   *   current user.
   *
   * @throws \Exception
   */
  public function historyWriteTimestamp($nid, $timestamp, $account = NULL) {

    if (!isset($account)) {
      $account = \Drupal::currentUser();
    }

    if ($account->isAuthenticated()) {
      \Drupal::database()->merge('history')
        ->keys([
          'uid' => $account->id(),
          'nid' => $nid,
        ])
        ->fields(['timestamp' => $timestamp])
        ->execute();
      // Update static cache.
      $history = &drupal_static('history_read_multiple', []);
      $history[$nid] = $timestamp;
    }
  }

  /**
   * Check if object is node and account has view access.
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object instanceof NodeInterface) {
      $access = $object->access('view', $account, TRUE);
    }
    else {
      $access = AccessResult::forbidden();
    }
    return $return_as_object ? $access : $access->isAllowed();
  }

}
